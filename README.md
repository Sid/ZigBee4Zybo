The main goal of this projet is to integrate the ZigBee protocol within the
Xilinx Zybo Board and of course to port the software stack that Microchip provides for the selected
protocol to the ARM processor of the Zybo.
As a first thing a custom spi-master controller has been designed and then wrapped with an AXI slave interface in order
to become a peripheral. After that a kernel which supports IEEE 8002.15 has been compiled and also a specific
device tree has been written by hand in order to provide to Linux OS a logical and hierarchical view of the connected hardware.
# Table of content
* [License](#License)
* [Content](#Content)
* [Description](#Description)
* [Build everything from scratch](#Build)
    * [Downloads](#BuildDownloads)
    * [Hardware synthesis](#BuildSynthesis)
    * [Build a root file system](#BuildRootFs)
    * [Build the Linux kernel](#BuildKernel)
    * [Build U-Boot](#BuildUboot)
    * [Build the hardware dependant software](#BuildHwDepSw)
* [Test ZigBee protocol on the Zybo](#Run)
* [Going further](#Further)
    * [Possible improvements](#FurtherNetwork)

# <a name="License"></a>License
Copyright Telecom ParisTech  
Copyright Alessio Alberti  (alberti@eurecom.fr)
Copyright Simone Marchisio (marchisi@eurecom.fr)

Licensed uder the GPL.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

# <a name="Content"></a>Content

    .
    ├── C                           C source code
    │   ├── mrf24j40.c              Driver for MRF24J40
    |   ├── Makefile                To create kernel objects
    │   ├── SPI-peripheral2_0.c     Hand-written driver for spi-controller
    |   └── SPI-peripheral2_0.h     Header for spi-driver
    ├── ConfigKernel                Configuration for the kernel menuconfig
    ├── ConfigRootFs                Configuration for the rootfs menuconfig
    ├── COPYING                     License (English version)
    ├── hdl                         VHDL source code
    │   ├── axi_pkg.vhd             Package of AXI definitions
    │   ├── spi_master.vhd          Simple FSM implementation of controller
    │   └── SPI_peripheral.vhd      Top-level entity
    ├── Makefile                    Main makefile (automatically invoked by prepareAll.sh)
    ├── pl.dtsi                     Custom device tree structure
    ├── prepareAll.sh               Main script file
    ├── README.md                   This file
    └── scripts                     Scripts
        ├── boot.bif                Zynq Boot Image description File
        ├── dts.tcl                 TCL script for device tree generation
        ├── fsbl.tcl                TCL script for FSBL generation
        ├── ila.tcl                 TCL script for ILA debug cores
        ├── init.sh                 Initialize the Operating System running on ZyBo
        └── vvsyn.tcl               Vivado TCL synthesis script

# <a name="Description"></a>Description
For this project a custom SPI controller was written in VHDL, and then integrated
within simple AXI slave interface which is used by the processing system to configure
and to monitor the status of this new peripheral. Obviously also a custom linux device driver
was written to allow the operating system to better manage the peripheral and to integrate it.
The custom SPI controller has to communicate data with a PmodRF2 IEEE 802.15 RF Transceiver
(https://reference.digilentinc.com/pmod/pmod/rf2) which provides is designed for use with
the Microchip microcontroller families (MRF24J40) and the ZigBee software stack.
MRF24J40 is a complete IEEE 802.15.4 radio and operates in the 2.4GHz freq band. 
It supports ZigBee, MiWi and proprietary protocols to provide an ideal solution for
define a simple and low consumption wireless network between embedded systems (http://www.microchip.com/wwwproducts/en/MRF24J40)
Most of the time was spent to analyze the existing MRF24J40 driver and to understand how it
interact with a general spi driver, in order to create our custom implementation of it.
Another critical point was to properly define the linux device tree, in order to give to operating system
a hierachical view of the connected hardware and also to manage interrupts with came from RF module.
After that new root filesystem and kernel have been compiled in order to support our custom spi controller and 
the MRF24J40 module, a useful configuration script (init.sh) was put in the root filesystem in order to
configure insert required kernel modules and configure the WPAN connection which is used by the izchat program
that allow message passing between two boards.

# <a name="Build"></a>Build everything from scratch

In the following we will progressively build a complete computer system based on the Zybo board, with custom hardware extensions, and running a GNU/Linux operating system.

---

**Note**: You will need the Xilinx tools (Vivado and its companion SDK) and a PmodRF2 IEEE 802.15 RF. In the following we assume that they are properly installed. You will also need to download, configure and build several free and open source software. Some steps can be run in parallel because they do not depend on the results of other steps. Some steps cannot be started before others finish and this will be signalled.

---

**Note**: the instructions bellow introduce new concepts one after the other, with the advantage that for any given goal the number of actions to perform is limited and each action is easier to understand. The drawback is that, each time we introduce a new concept, we will have to reconfigure, rebuild and reinstall one or several components. In certain circumstances (like, for instance, when reconfiguring the Buildroot tool chain) such a move will take several minutes or even worse. Just the once will not hurt, the impatient should thus read the complete document before she starts following the instructions.

---

**Note**: the Linux repository, the Buildroot repository and the directory in which we will build all components occupy several GB of disk space each. Carefully select where to install them.

## <a name="BuildDownloads"></a>Downloads

First define shell environment variables pointing to the local copies of the various git repositories we need. Their approximate respective sizes are provided in comments to help you deciding where to put them. Note that they will potentially grow a bit each time you will pull the last commits from the remote repositories. Note also that we will build everything in a sub-directory of `$SPI_P` and that the total size of `$SPI_P` will increase a lot during the build.
To do that a useful way is to modify the .bashrc file which is in the home directory adding the following lines, then save the file and close it.
Each time a shell is open, it has the visibility of these variables.

To modify the file you can time for example:

    $ nano .bashrc (or alternatively gedit .bashrc)

    -- Add the following lines

    export SPI_P=<some-path>/ZigBee4Zybo      # 3.3GB after all builds
    export XLINUX=<some-path>/linux-xlnx      # 2.3GB
    export XUBOOT=<some-path>/u-boot-xlnx     # 270MB
    export XDTS=<some-path>/device-tree-xlnx  # 2MB
    export BUILDROOT=<some-path>/buildroot    # 170MB

After that the shell has to be closed and then restarted. (To make variables effective)
Clone all components from their respective git repositories:

    $ git clone https://gitlab.eurecom.fr/Sid/ZigBee4Zybo.git $SPI_P
    $ git clone https://github.com/Xilinx/linux-xlnx.git $XLINUX
    $ git clone https://github.com/Xilinx/u-boot-xlnx.git $XUBOOT
    $ git clone http://github.com/Xilinx/device-tree-xlnx.git $XDTS
    $ git clone http://git.buildroot.net/git/buildroot.git $BUILDROOT

## <a name="BuildSynthesis"></a>Hardware synthesis

The hardware synthesis produces a bitstream file from the VHDL source code (in `$SPI_P/hdl`). It is done by the Xilinx Vivado tools. SPI_P comes with a Makefile and a synthesis script that automate the synthesis:

    $ cd $SAB4Z
    $ make vv-all

The generated bitstream is `$SPI_P/build/vv/top.runs/impl_1/top_wrapper.bit`. This binary file is used to configure the FPGA part of the Zynq core of the Zybo board such that it implements our VHDL design. A binary description of our hardware design is also available in `$SPI_P/build/vv/top.runs/impl_1/top_wrapper.sysdef`. It is not human-readable but we will use it later to generate the device tree sources and the First Stage Boot Loader (FSBL) sources.

---

**Note**: we will also use the `$SPI_P/build` directory to build and store all other components. It is the only time that we call directly the Makefile.

---

## <a name="BuildRootFs"></a>Build a root file system

Just like your host, our Zybo computer needs a root filesystem to run a decent operating system like GNU/Linux. We will use [Buildroot](https://buildroot.org/) to build and populated a Busybox-based, tiny, initramfs root file-system. In other notes will will explore other types of root file-system (networked file-system, file-system on the MicroSD card...) but as initramfs is probably the simplest of all, let us start with this one.

Buildroot is a very nice and easy to use toolbox dedicated to the creation of root file-system for many different target embedded systems. It can also build Linux kernels and other useful software but we will use it only to generate our root file-system. Buildroot has no default configuration for the Zybo board but the ZedBoard (another very common board based on Xilinx Zynq cores) default configuration works also for the Zybo. First create a default Buildroot configuration in `$SPI_P/build/rootfs` for our board:

From this point we dont refer directly to the Makefile but to the prepareAll.sh script, which makes our configuration as simple as possible, so to build the root file-system we only have to type the following commands:

    $ cd $SPI_P
    $ chmod u+x prepareAll.sh
    $ ./prepareAll.sh rootfs

**Note**: A question is raised during the process, and you should asware n (no!).

The last command call the script in order to create the root file-system, and of course the piece of code in charge to do this is reported and explained below. You don't need to modify it, but of course you can do it if you want.

    mkdir -p $SPI_P/build/rootfs
	touch $SPI_P/build/rootfs/external.mk $SPI_P/build/rootfs/Config.in
	cd $BUILDROOT
	make BR2_EXTERNAL=$SPI_P/build/rootfs O=$SPI_P/build/rootfs zynq_zed_defconfig	
	cp $SPI_P/ConfigRootFs $SPI_P/build/rootfs/.config 
	mkdir -p $SPI_P/build/rootfs/overlays/etc/profile.d
	echo "export PS1='Spi_p> '" > $SPI_P/build/rootfs/overlays/etc/profile.d/prompt.sh
	cp $SPI_P/scripts/init.sh $SPI_P/build/rootfs/overlays
	cd $SPI_P/build/rootfs
	make 

This code call the proper makefiles and set the destination for the built root file-system. An important aspect is that the file ConfigRootFs is taken as .config file by BusyBox, and since it contains a working configuration it allow us to avoid to do the menuconfig each time.
The file $SPI_P/scripts/init.sh is copied in the overlays directory and it is used by the Linux Operating system to insert required modules, configure WPAN and start the izchat.
 The Buildroot overlays directory is the perfect way to embed files, scripts, kernel modules in the root file system. Of course, we will have to rebuild the root file system but this should be very fast.

**Note**: the first build takes some time, especially because Buildroot must first build the toolchain for ARM targets, but most of the work will not have to be redone if we later change the configuration and rebuild (unless we change the configuration of the Buildroot tool chain, in which case we will have to rebuild everything).

The generated root file-system is available in different formats:

* `$SPI_P/build/rootfs/images/rootfs.tar` is a tar archive that we can explore with the tar utility. This is left as an exercise for the reader.
* `$SPI_P/build/rootfs/images/rootfs.cpio` is the same but in cpio format (another archive format).
* `$SPI_Pbuild/rootfs/images/rootfs.cpio.gz` is the compressed version of `$SPI_P/build/rootfs/images/rootfs.cpio`.
* Finally, `$SPI_P/build/rootfs/images/rootfs.cpio.uboot` is the same as `$SPI_P/build/rootfs/images/rootfs.cpio.gz` with a 64 bytes header added.

Buildroot also built applications for the host PC that we will need later:

* a complete toolchain (cross-compiler, debugger...) for the ARM processor of the Zybo,
* dtc, a device tree compiler (more on this later),
* mkimage, a utility used to create images for U-Boot (and that the build system used to create `$SPI_P/build/rootfs/images/rootfs.cpio.uboot`).

## <a name="BuildKernel"></a>Build the Linux kernel

---

**Note**: Do not start this part before the toolchain is built: it is needed.

---

Run the Linux kernel configurator to create a default kernel configuration for our board in `$SPI_P/build/kernel. Also in this case we use the $SPI_P/prepareAll.sh script which will compile the kernel within just one command.

    $ cd $SPI_P
    $ ./prepareAll.sh kernel

Now a brief explanation is given for the code which compile the proper kernel.

    export PATH=$PATH:$SPI_P/build/rootfs/host/usr/bin
	export CROSS_COMPILE=arm-buildroot-linux-uclibcgnueabi-
	cd $XLINUX
	make O=$SPI_P/build/kernel ARCH=arm xilinx_zynq_defconfig
	cp -f $SPI_P/ConfigKernel $SPI_P/build/kernel/.config
	cd $SPI_P/build/kernel
	make -j8 ARCH=arm
	make ARCH=arm LOADADDR=0x8000 uImage
	make -j8 ARCH=arm modules
	make -j8 ARCH=arm modules_install INSTALL_MOD_PATH=$SPI_P/build/rootfs/overlays
	make headers_install ARCH=arm INSTALL_HDR_PATH=$SPI_P/build/rootfs/overlays
	cd $SPI_P/C
	make
	cd $SPI_P
	wget http://www.infradead.org/~tgr/libnl/files/libnl-3.2.24.tar.gz
	tar -xzf libnl-3.2.24.tar.gz
	cd libnl-3.2.24
	./configure --host=arm-linux --prefix=$SPI_P/build/rootfs/overlays
	make
	make install
	git clone git://git.code.sf.net/p/linux-zigbee/linux-zigbee linux-zigbee
	cd linux-zigbee
	./autogen.sh
	./configure --prefix=$SPI_P/build/rootfs/overlays --host=arm-linux PKG_CONFIG_PATH=$SPI_P/build/rootfs/overlays/lib/pkgconfig
	make
	make install
	rm -rf $SPI_P/libnl-3.2.24.tar.gz
	cd $SPI_P/build/rootfs
	make

The first 2 commands add the directory which contains the complete toolchain (cross-compiler, debugger...) for the ARM processor of the Zybo and set the $CROSS_COMPILER which is used to compile for the ARM architecture.
The Linux kernel build system offers a way to tune the default configuration before building, and as well as in buildroot we use the file $SPI_P/ConfigKernel to use the proper configuration and make the process faster. More in detail the file ConfigKernel add the fully support for the IEEE 802.15 protocol.

As for the root file-system, the kernel is available in different formats:

* `$SPI_P/build/kernel/vmlinux` is an uncompressed executable in ELF format,
* `$SPI_P/build/kernel/arch/arm/boot/zImage` is a compressed executable.
And just like for the root file-system, none of these is the one we will use on the Zybo. In order to load the kernel in memory with U-Boot we must generate a kernel image in U-Boot format. This can be done using the Linux kernel build system. We just need to provide the load address and entry point that U-Boot will use when loading the kernel into memory and when jumping into the kernel

    make ARCH=arm LOADADDR=0x8000 uImage

The result is in `$SPI_P/build/kernel/arch/arm/boot/uImage.

When loading the kernel, U-Boot will copy the archive somewhere in memory, parse the 64 bytes header, uncompress the kernel and install it starting at address `0x8000`, add some more information in the first 32 kB of memory and jump at the entry point, which is also `0x8000`. This kernel image is thus the one we will store on the MicroSD card and use on the Zybo.

The kernel embeds a collection of software device drivers that are responsible for the management of the various hardware devices (network interface, timer, interrupt controller...) Some are integrated into the kernel, some are delivered as _external modules_, a kind of device driver that is dynamically loaded in memory by the kernel when it is needed. These external modules must also be built and installed in our root file-system where the kernel will find them, that is, in `/lib/modules`. The Buildroot overlays directory is the perfect way to embed the kernel modules in the root file-system. Of course, we will have to rebuild the root file-system but this should be very fast.
In this case libnl and linux-zigbee software stack are installed in $SPI_P/build/rootfs/overlays (You can find their executable inside the folders bin and sbin).

## <a name="BuildUboot"></a>Build U-Boot

---

**Note**: Do not start this part before the toolchain is built: it is needed.

---

U-Boot is a boot loader that is very frequently used in embedded systems. It runs before the Linux kernel to:

* initialize the board,
* load the initramfs root file-system image in RAM,
* load the device-tree blob in RAM (more on this later),
* load the Linux kernel image in RAM,
* parse the header of the Linux kernel image,
* uncompress the Linux kernel and install it at the load address specified in the header,
* prepare some parameters for the kernel in CPU registers and in the first 32 kB of memory,
* jump into the kernel at the specified entry point.

To configure and build U-boot in `$SPI_P/build/uboot` we just need to lunch the following commands:

    $ cd $SPI_P
    $ ./prepareAll.sh uboot

Just like Buildroot and the Linux kernel, the U-Boot build system offers a way to tune the default configuration before building, but in this case we use the default one, in fact there isn't a uboot configuration file.
You can find the code to build uboot into prepareAll.sh script, it is quite simple.

Again, the result of U-Boot build is available in different formats. The one we are interested in and that we will use on the Zybo is the executable in ELF format, `$SPI_P/build/uboot/u-boot`. Later, we will glue it together with several other files to create a single _boot image_ file. As the Xilinx bootgen utility that we will use for that insists that its extension is `.elf`. The code in prepareAll.sh automatically rename the output file.

## <a name="BuildHwDepSw"></a>Build the hardware dependant software

Do not start this part before the [hardware synthesis finishes](#BuildSynthesis) finishes and the [toolchain](#BuildRootFs) is built: they are needed.

### <a name="BuildHWDepSWDTS"></a>Linux kernel device tree

ZigBee4Zybo comes with a Makefile and a TCL script that automate the generation of device tree sources using the Xilinx hsi utility, the clone of the git repository of Xilinx device trees (`<some-path>/device-tree-xlnx`) and the description of our hardware design that was generated during the hardware synthesis (`$SPI_P/build/vv/top.runs/impl_1/top_wrapper.sysdef`). The script prepareAll.sh interact with this make file in a very efficient way and so to compile the device tree we just need to run the following commands:

    $ cd $SSPI_P
    $ ./prepareAll.sh dts

The sources are in `$SPI_P/build/dts`, the top level is `$SPI_P/build/dts/system.dts`. After the sources generation but before the compilation, the prepareAll.sh script substitute the automatically generated file `$SPI_P/build/dts/pl.dtsi` with our custom and hand-written `$SPI_P/pl.dtsi`. This substitution has to be done sice the our file provide to Linux operating system information about how the ardware is connected together and also about interrupts.
It is very important have a look on this file, since it took a lot of time to be written. After that the script buil the device tree and the output can be found in `$SPI_P/build/devicetree.dtb`.


### <a name="BuildHWDepSWFSBL"></a>First Stage Boot Loader (FSBL)

Generate the FSBL sources

    Host-Xilinx> cd $SAB4Z
    Host-Xilinx> make fsbl

The sources are in `$SAB4Z/build/fsbl`. If needed, edit them before compiling the FSBL:

    Host-Xilinx> cd $SAB4Z
    Host-Xilinx> make -C build/fsbl

### <a name="BuildHWDepSWBootImg"></a>Zynq boot image

A Zynq boot image is a file that is read from the boot medium of the Zybo when the board is powered on. It contains the FSBL ELF, the bitstream and the U-Boot ELF. Generate the Zynq boot image with the Xilinx bootgen utility and the provided boot image description file:

    $ cd $PI_P
    $ ./prepareAll fsbl

### <a name="BuildHWDepSWSDCard"></a>Prepare the MicroSD card

Finally, prepare a MicroSD card with a FAT32 first primary partition (8MB minimum), mount it on your host PC, and copy the different components to it. To do this the prepareAll script assume the SD card is mounted under `/media`, so it just needs of its name.
The script automatically perform the unmount command.

    $ cd $SPI_P
    $ ./prepareAll sd <name-of-mounted-sd-card>
    

Eject the MicroSD card, plug it in the Zybo and power on.

# <a name="Run"></a>Test ZigBee4Zybo on the Zybo

* Plug the MicroSD card in the Zybo and connect the USB cable.
* Check the position of the jumper that selects the power source (USB or power adapter).
* Check the position of the jumper that selects the boot medium (MicroSD card).
* Power on. Two new  should show up (`/dev/ttyUSB0` and `/dev/ttyUSB1` by default) on the host PC. `/dev/ttyUSB1` is teh one corresponding to the serial link with the Zybo.
* Launch a terminal emulator (picocom, minicom...) and attach it to the new character device, with a 115200 baudrate, no flow control, no parity, 8 bits characters, no port reset and no port locking: `picocom -b115200 -fn -pn -d8 -r -l /dev/ttyUSB1`.
* Wait until Linux boots, log in as root (there is no password) and start interacting with ZigBee4Zybo.

<!-- -->
    Host> picocom -b115200 -fn -pn -d8 -r -l /dev/ttyUSB1
    ...
    Welcome to Buildroot
    Spi_p login: root
    Spi_p> 

At this point we have to run the init.sh script which we put in the `$SPI_P/build/rootfs/overlays` directory.
This script contains all needed commands to insert modules (one for ou custom spi controlle and the other for the MRF24J40) and configure WPAN (Wireless Personal Area Network).
As last instruction it runs the program `izchat` that allow 2 boards (with related PmodRF2) to chat together.

    cd ..
    ./init.sh 8001 8002
    (./init.sh 8002 8001 -> for the other board !!!)
    
After that we can write message to send to the other board and receive from it.

---

**Note**: Sometimes may happen that the init.sh script is blocking and in that case you have to restart the board and then enter in the script folder and run its command once at a time.

---

# <a name="Further"></a>Going further

## <a name="FurtherNetwork"></a>Possible Improvements

The VHDL design of the spi-master controller is able to drive up to 4 slaves, so a possible improvement is to adapt its driver in order to really manage more slaves.

Another improvent would be write a more complex user application that exlploits ZigBee, such as example file-sharing, instead of using the simple izchat.
In this case would also be possible to perform analysis on the boudrate.