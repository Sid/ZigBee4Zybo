#	This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

proc usage {} {
	puts "usage: vivado -mode batch -source <script> -tclargs <rootdir> <builddir> \[<ila>\]"
	puts "  <rootdir>:  absolute path of SPI_peripheral root directory"
	puts "  <builddir>: absolute path of build directory"
	puts "  <ila>:      embed Integrated Logic Analyzer (0 or 1, default 0)"
	exit -1
}

if { $argc == 3 } {
	set rootdir [lindex $argv 0]
	set builddir [lindex $argv 1]
	set ila [lindex $argv 2]
	if { $ila != 0 && $ila != 1 } {
		usage
	}
} else {
	usage
}

cd $builddir
source $rootdir/scripts/ila.tcl

###################
# Create SPI_peripheral IP #
###################
create_project -part xc7z010clg400-1 -force SPI_peripheral SPI_peripheral
add_files $rootdir/hdl/axi_pkg.vhd $rootdir/hdl/spi_master.vhd $rootdir/hdl/SPI_peripheral.vhd
import_files -force -norecurse
ipx::package_project -root_dir SPI_peripheral -vendor www.telecom-paristech.fr -library SPI_PERIPHERAL -force SPI_peripheral
close_project

############################
## Create top level design #
############################
set top top
create_project -part xc7z010clg400-1 -force $top .
set_property board_part digilentinc.com:zybo:part0:1.0 [current_project]
set_property ip_repo_paths { ./SPI_peripheral } [current_fileset]
update_ip_catalog
create_bd_design "$top"
set SPI_peripheral [create_bd_cell -type ip -vlnv [get_ipdefs *www.telecom-paristech.fr:SPI_PERIPHERAL:SPI_peripheral:*] SPI_peripheral]
set ps7 [create_bd_cell -type ip -vlnv [get_ipdefs *xilinx.com:ip:processing_system7:*] ps7]
set_property -dict [list CONFIG.PCW_USE_M_AXI_GP0 {1}] $ps7
set_property -dict [list CONFIG.PCW_M_AXI_GP0_ENABLE_STATIC_REMAP {1}] $ps7
set_property -dict [list CONFIG.PCW_USE_FABRIC_INTERRUPT {1}] $ps7
set_property -dict [list CONFIG.PCW_IRQ_F2P_INTR {1}] $ps7
set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {100}] $ps7
set xlConcat [create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0]
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" } $ps7

# Interconnections
#xlConcat
connect_bd_net [get_bd_pins $xlConcat/dout] [get_bd_pins ps7/IRQ_F2P]
# ex_int
create_bd_port -dir I -type intr ex_int
set_property CONFIG.SENSITIVITY LEVEL_HIGH [get_bd_ports ex_int]
connect_bd_net [get_bd_ports ex_int] [get_bd_pins $xlConcat/In1]

# RST
create_bd_port -dir O -type rst RST
connect_bd_net [get_bd_ports RST] [get_bd_pins $ps7/FCLK_RESET0_N]
# INTERRUPT
connect_bd_net [get_bd_pins $SPI_peripheral/irq] [get_bd_pins $xlConcat/In0]

# SPI controller
create_bd_port -dir I MISO
connect_bd_net [get_bd_pins $SPI_peripheral/miso] [get_bd_ports MISO]
create_bd_port -dir O MOSI
connect_bd_net [get_bd_pins $SPI_peripheral/mosi] [get_bd_ports MOSI]
create_bd_port -dir O SCLK
connect_bd_net [get_bd_pins $SPI_peripheral/sclk] [get_bd_ports SCLK]
create_bd_port -dir O SS
connect_bd_net [get_bd_pins $SPI_peripheral/slave_sel] [get_bd_ports SS]

# ps7 - SPI_peripherals
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master "/ps7/M_AXI_GP0" Clk "Auto" }  [get_bd_intf_pins /SPI_peripheral/s0_axi]

# Addresses ranges
set_property range 4K [get_bd_addr_segs {/ps7/Data/SEG_SPI_peripheral_reg0}]
set_property offset 0x40000000 [get_bd_addr_segs {/ps7/Data/SEG_SPI_peripheral_reg0}]

set suffixes {
	SPI_peripheral_sclk
	SPI_peripheral_mosi
	MISO_1
	SPI_peripheral_slave_sel 
	ps7_FCLK_RESET0_N
	ex_int_1
	SPI_peripheral_irq
}
# In-circuit debugging
if { $ila == 1 } {
	#set_property HDL_ATTRIBUTE.MARK_DEBUG true [get_bd_intf_nets -of_objects [get_bd_intf_pins $SPI_peripheral/s0_axi]]
	foreach suffix $suffixes {
		set_property HDL_ATTRIBUTE.MARK_DEBUG true [get_bd_nets $suffix]
	}
}

# Synthesis flow
validate_bd_design
set files [get_files *$top.bd]
generate_target all $files
add_files -norecurse -force [make_wrapper -files $files -top]
save_bd_design
set run [get_runs synth*]
set_property STEPS.SYNTH_DESIGN.ARGS.FLATTEN_HIERARCHY none $run
launch_runs $run
wait_on_run $run
open_run $run

# In-circuit debugging
if { $ila == 1 } {
	set topcell [get_cells $top*]
	set nets {}
	foreach suffix $suffixes {
		lappend nets $topcell/${suffix}
	}
	add_ila_core dc $topcell/ps7_FCLK_CLK0 $nets
}

# IOs
array set ios {
	"SS[0]"		{ "V12" "LVCMOS33" }
	"MOSI"		{ "W16" "LVCMOS33" }
	"MISO"		{ "J15" "LVCMOS33" }
	"SCLK"		{ "H15" "LVCMOS33" }
	"ex_int"	{ "V13" "LVCMOS33" }
	"RST"		{ "U17" "LVCMOS33" }
}
foreach io [ array names ios ] {
	set pin [ lindex $ios($io) 0 ]
	set std [ lindex $ios($io) 1 ]
	set_property package_pin $pin [get_ports $io]
	set_property iostandard $std [get_ports [list $io]]
}
# Timing constraints
set clock [get_clocks]
set_false_path -from $clock -to [get_ports {SCLK}]
set_false_path -from [get_ports {MISO}] -to $clock
set_false_path -from $clock -to [get_ports {MOSI}]
set_false_path -from $clock -to [get_ports {SS}]

# Implementation
save_constraints
set run [get_runs impl*]
reset_run $run
set_property STEPS.WRITE_BITSTREAM.ARGS.BIN_FILE true $run
launch_runs -to_step write_bitstream $run
wait_on_run $run

# Messages
set rundir ${builddir}/$top.runs/$run
puts ""
puts "\[VIVADO\]: done"
puts "  bitstream in $rundir/${top}_wrapper.bit"
puts "  resource utilization report in $rundir/${top}_wrapper_utilization_placed.rpt"
puts "  timing report in $rundir/${top}_wrapper_timing_summary_routed.rpt"
