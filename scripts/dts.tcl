#	This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

if { $argc != 3 } {
	puts "usage: hsi -mode batch -quiet -notrace -source dts.tcl -tclargs <hardware-description-file> <path-to-device-tree-xlnx> <local-device-tree-directory>"
} else {
	set hdf [lindex $argv 0]
	set xdts [lindex $argv 1]
	set ldts [lindex $argv 2]
	open_hw_design $hdf
	set_repo_path $xdts
	create_sw_design device-tree -os device_tree -proc ps7_cortexa9_0
	generate_target -dir $ldts
}
