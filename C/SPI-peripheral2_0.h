struct node{
	u8 data;
	struct node* next;
};

struct t_queue {
	int N;
	struct node *head, *tail;
};

static void Enqueue(struct t_queue* queue,u8 data);
static void Dequeue(struct t_queue* queue);
static u8 Head(struct t_queue* queue);
static int getPositionLSB(u16 number);