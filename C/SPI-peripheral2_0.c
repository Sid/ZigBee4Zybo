/*
--	This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--    This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--   You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#define DEBUG

#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/spi/spi_bitbang.h>
#include <linux/io.h>
#include "SPI-peripheral2_0.h"

#define SPI_PERIPHERAL_NAME "spi_peripheral"

/* Register definitions as per "OPB Serial Peripheral Interface (SPI) (v1.00e)
 * Product Specification", DS464
 */
#define SPI_TXD_OFFSET			0x00	/* Data Transmit Register */
#define SPI_RXD_OFFSET			0x04	/* Data Receive Register */
#define SPI_DIV_OFFSET			0x08	/* Divisor Register */
#define SPI_CR_OFFSET			0x0C	/* Control Register */

#define SPI_CREG_CPHA			0x01
#define SPI_CREG_CPOL			0x02
#define SPI_CREG_ACK			0x04
#define SPI_CREG_ENABLE			0x08
#define SPI_CREG_RST			0x10
#define SPI_CREG_SS				0x60
#define SPI_CREG_MASK			(SPI_CREG_CPHA | SPI_CREG_CPOL | \
 	SPI_CREG_ACK | SPI_CREG_ENABLE | SPI_CREG_RST | SPI_CREG_SS)

#define XSPI_SR_RX_EMPTY_MASK	0x01	// Receive FIFO is empty
/*#define XSPI_SR_RX_FULL_MASK	0x02	// Receive FIFO is full
#define XSPI_SR_TX_EMPTY_MASK	0x04	// Transmit FIFO is empty
#define XSPI_SR_TX_FULL_MASK	0x08	// Transmit FIFO is full
#define XSPI_SR_MODE_FAULT_MASK	0x10	// Mode fault error
*/
#define SPI_DIVISOR				0x5
#define MAX_FIFO				64

struct spi_peripheral {
	/* bitbang has to be first */
	struct spi_bitbang bitbang;
	struct completion done;
	void __iomem	*regs;	/* virt. address of the control registers */

	int		irq;

	u8 *rx_ptr;		/* pointer in the Tx buffer */
	const u8 *tx_ptr;	/* pointer in the Rx buffer */
	int tx_bytes, rx_bytes;
	u8 bytes_per_word;
	int buffer_size;	/* buffer size in words */
	struct t_queue *queue;
	u8 cs_inactive;	/* Level of the CS pins when inactive*/
	unsigned int (*read_fn)(void __iomem *);
	void (*write_fn)(u32, void __iomem *);
};

#define printdev(X) (&X->bitbang.master->dev)

static void spi_peripheral_write32(u32 val, void __iomem *addr)
{
	iowrite32(val, addr);
}

static unsigned int spi_peripheral_read32(void __iomem *addr)
{
	return ioread32(addr);
}


static int spi_peripheral_transfer_one(struct spi_master *master,
			     struct spi_device *spi,
			     struct spi_transfer *t)
{
	struct spi_peripheral *spi_p = spi_master_get_devdata(spi->master);
	u8 *buf;
	u16 cr;
	unsigned i;
	u32 rdata = 0;
	size_t left;

	if (t->tx_buf) {	
		//it has to transfer significant data
		buf = (u8 *)t->tx_buf;
		left = t->len;
		while (left) {
			//transfer 1 BYTE at the time
			size_t to_write = min_t(size_t, 1, left);
			reinit_completion(&spi_p->done);
			spi_p->write_fn(*buf, spi_p->regs + SPI_TXD_OFFSET);
			cr = spi_p->read_fn(spi_p->regs + SPI_CR_OFFSET);
			spi_p->write_fn(cr | SPI_CREG_ENABLE, spi_p->regs + SPI_CR_OFFSET);
			// Wait for SPI to finish
			wait_for_completion(&spi_p->done);
			// Enqueue read value (could receive while it is sending data)
			rdata = spi_p->read_fn(spi_p->regs + SPI_RXD_OFFSET);
			Enqueue(spi_p->queue, (u8) rdata);
			left -= to_write;
			buf += to_write;
		}
	} else{
		//it has to receive data (transmits 0)
		for(i=0; i<t->len; i++){
			reinit_completion(&spi_p->done);
			spi_p->write_fn(0, spi_p->regs + SPI_TXD_OFFSET);
			cr = spi_p->read_fn(spi_p->regs + SPI_CR_OFFSET);
			spi_p->write_fn(cr | SPI_CREG_ENABLE, spi_p->regs + SPI_CR_OFFSET);
				
			// Wait for SPI to finish 
			wait_for_completion(&spi_p->done);
			rdata = spi_p->read_fn(spi_p->regs + SPI_RXD_OFFSET);
			Enqueue(spi_p->queue, (u8) rdata);
		}
	}

	if (t->rx_buf) {
		//it has to receive data
		buf = (u8 *)t->rx_buf;
		left = t->len;
		while (left) {
			size_t to_read = min_t(size_t, 1, left);
			rdata = Head(spi_p->queue);
			Dequeue(spi_p->queue);
			*buf = rdata;
			left -= to_read;
			buf += to_read;
		}
	}else{
		//called when the received data are not useful -> DEQUEUE 
		for(i=0; i<t->len; i++){
			Dequeue(spi_p->queue);
		}
	}
	return 0;
}

static void spi_peripheral_init_hw(struct spi_peripheral *spi_p)
{
	void __iomem *regs_base = spi_p->regs;
	u16 cr, cs;

	/* Reset the SPI device */
	spi_p->write_fn(SPI_CREG_RST, regs_base + SPI_CR_OFFSET);
	/* Set the divisor */
	spi_p->write_fn(SPI_DIVISOR, regs_base + SPI_DIV_OFFSET);
	cr = spi_p->read_fn(spi_p->regs + SPI_CR_OFFSET);
	cs = spi_p->cs_inactive & (SPI_CREG_SS >> getPositionLSB(SPI_CREG_SS));
	cs <<= getPositionLSB(SPI_CREG_SS); 
	spi_p->write_fn(cr, spi_p->regs + SPI_CR_OFFSET);
	/* Initialize the FIFO Rx */
	spi_p->queue = kmalloc(sizeof(struct t_queue), GFP_KERNEL);
	if(spi_p->queue == NULL){
		printk(KERN_ERR "error allocating queue\n");
		return;
	}
	spi_p->queue->N = 0;
	spi_p->queue->head = NULL;
	spi_p->queue->tail = NULL;
	return;
}

static int getPositionLSB(u16 number){
	int index = 0;
	while((~number)&1){
		number>>=1;
		index++;
	}
	return index;
}
/***************************
*
*called when the chip_select signal has to be enabled/disabled
*
****************************/
static void spi_peripheral_chipselect(struct spi_device *spi, bool is_on)
{
	struct spi_peripheral *spi_p = spi_master_get_devdata(spi->master);
	u16 cr = 0;
	u8	cs;

	/* Set the SPI clock phase and polarity */
	cr = spi_p->read_fn(spi_p->regs + SPI_CR_OFFSET) & ~SPI_CREG_MASK;
	dev_dbg(printdev(spi_p), "cr %04x\n", cr);

	// Set the SPI clock phase and polarity
	if (spi->mode & SPI_CPHA)
		cr |= SPI_CREG_CPHA;
	if (spi->mode & SPI_CPOL)
		cr |= SPI_CREG_CPOL;
	if(is_on){
		cr = spi_p->read_fn(spi_p->regs + SPI_CR_OFFSET);
		cs = spi_p->cs_inactive & (SPI_CREG_SS >> getPositionLSB(SPI_CREG_SS));
		cs <<= getPositionLSB(SPI_CREG_SS); 
		cr |= cs;
		spi_p->write_fn(cr, spi_p->regs + SPI_CR_OFFSET);
	} else {
		cs = 1 << getPositionLSB(SPI_CREG_SS); 
		cs <<= spi->chip_select;
		cr &= ~cs;
	}
	// Activate the chip select 
	spi_p->write_fn(cr, spi_p->regs + SPI_CR_OFFSET);
}

/*********************************
*
*called when the transfer has to be configured
*
**********************************/
static int spi_peripheral_setup_transfer(struct spi_master *spi)
{
	//printk(KERN_INFO "setup transfer\n");
	return 0;
}

/**********************************
*
*called when the message to be transferred is known
*
**********************************/ 
static int spi_peripheral_prepare_message(struct spi_master *master, struct spi_message *msg)
{
	//printk(KERN_INFO "prepare message, length = %u %u\n", msg->actual_length, msg->frame_length);
	return 0;
}

/**********************************
*
*called after the transfer of a frame is completed
*
***********************************/
static int  spi_peripheral_unsetup_transfer(struct spi_master *spi)
{	
	//printk(KERN_INFO "unsetup transfer\n");
	return 0;
}

/**********************************
*
*enqueue in tail
*
***********************************/
static void Enqueue(struct t_queue* queue,u8 data){
	struct node* node;
	if(queue->N > MAX_FIFO){
		Dequeue(queue);
	}
	node = kmalloc(sizeof(struct node), GFP_KERNEL);
	if(!node){
		printk(KERN_ERR "error allocating a new node\n");
		return;
	}
	node->data = data;
	node->next = NULL;
	if(queue->head == NULL && queue->tail == NULL){
		queue->N = 1;
		queue->head = queue->tail = node;
		return;
	}
	queue->tail->next = node;
	queue->tail = node;
	queue->N++;
	return;
}
/**********************************
*
*dequeue from head
*
**********************************/
static void Dequeue(struct t_queue* queue){
	struct node* node = queue->head;
	if(queue->head == NULL){
		return;
	}
	if(queue->head == queue->tail){
		queue->N = 0;
		queue->head = queue->tail = NULL;
	} else {
		queue->N--;
		queue->head = queue->head->next;
	}
	kfree(node);
}

/**********************************
*
*read the value in the HEAD
*
**********************************/
static u8 Head(struct t_queue* queue){
	if(queue->head == NULL){
		return -1;
	}
	return queue->head->data;
}

/**********************************
*
*IRQ callback
*
***********************************/
static irqreturn_t spi_peripheral_irq(int irq, void *dev_id)
{
	struct spi_peripheral *spi_p = dev_id;
	u16 cr;

	dev_dbg(printdev(spi_p),"IRQ SPI_controller");
	// set ACK
	cr = spi_p->read_fn(spi_p->regs + SPI_CR_OFFSET);
	spi_p->write_fn(cr | SPI_CREG_ACK, spi_p->regs + SPI_CR_OFFSET);
	complete(&spi_p->done);

	return IRQ_HANDLED;
}

static const struct of_device_id spi_peripheral_of_match[] = {
	{ .compatible = "xlnx,SPI-peripheral-1.0", },
	{}
};
MODULE_DEVICE_TABLE(of, spi_peripheral_of_match);

static int spi_peripheral_probe(struct platform_device *pdev)
{
	struct spi_peripheral *spi_p;
	struct resource *res;
	int ret, num_cs = 1, bits_per_word = 8;
	struct spi_master *master;
	
	of_property_read_u32(pdev->dev.of_node, "num-ss-bits", &num_cs);

	if (!num_cs) {
		dev_err(&pdev->dev, "Missing slave select configuration data\n");
		return -EINVAL;
	}

	dev_dbg(&pdev->dev, "num-ss-bits %d\n",num_cs);

	if (num_cs <0 || num_cs> 4) {
		dev_err(&pdev->dev, "Invalid number of spi slaves\n");
		return -EINVAL;
	}

	master = spi_alloc_master(&pdev->dev, sizeof(struct spi_peripheral));
	if (!master)
		return -ENODEV;

	/* the spi->mode bits understood by this driver: */

	spi_p = spi_master_get_devdata(master);
	dev_dbg(&pdev->dev, "PROBE of SPI-peripheral with mode bits %u\n", master->mode_bits);

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	//get registers
	spi_p->regs = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(spi_p->regs)) {
		ret = PTR_ERR(spi_p->regs);
		goto put_master;
	}
	spi_p->cs_inactive = 0xff;
	master->transfer_one = spi_peripheral_transfer_one;
	master->prepare_message = spi_peripheral_prepare_message;
	master->prepare_transfer_hardware = spi_peripheral_setup_transfer;
	master->unprepare_transfer_hardware = spi_peripheral_unsetup_transfer;
	master->set_cs = spi_peripheral_chipselect;
	master->bus_num = pdev->id;
	master->num_chipselect = num_cs;
	master->dev.of_node = pdev->dev.of_node;
	master->bits_per_word_mask = SPI_BPW_MASK(8);
	master->mode_bits = SPI_CPOL | SPI_CPHA | SPI_CS_HIGH;
	platform_set_drvdata(pdev, master);
	init_completion(&spi_p->done);

	spi_p->read_fn = spi_peripheral_read32;
	spi_p->write_fn = spi_peripheral_write32;

	spi_p->irq = platform_get_irq(pdev, 0);
	
	if (spi_p->irq >= 0) {
		/* Register for SPI Interrupt */
		ret = devm_request_irq(&pdev->dev, spi_p->irq, spi_peripheral_irq, 0,dev_name(&pdev->dev), spi_p);
		if (ret)
			goto put_master;
	}

	/* SPI controller initializations */
	spi_peripheral_init_hw(spi_p);

	ret = spi_register_master(master);
	if (ret) {
		goto put_master;
	}

	dev_info(&pdev->dev, "at 0x%08llX mapped to 0x%p\n",(unsigned long long)res->start, spi_p->regs);
	return 0;

put_master:
	spi_master_put(master);

	return ret;
}

static int spi_peripheral_remove(struct platform_device *pdev)
{
	struct spi_master *master = platform_get_drvdata(pdev);
	struct spi_peripheral *spi_p = spi_master_get_devdata(master);
	u16 cr;
	void __iomem *regs_base = spi_p->regs;

	spi_bitbang_stop(&spi_p->bitbang);

	/* Disable all the interrupts just in case */
	cr = spi_p->read_fn(regs_base + SPI_CR_OFFSET);
	spi_p->write_fn(cr | SPI_CREG_ACK, regs_base + SPI_CR_OFFSET);

	spi_master_put(spi_p->bitbang.master);
	return 0;
}

/* work with hotplug and coldplug */
MODULE_ALIAS("platform:" SPI_PERIPHERAL_NAME);

static struct platform_driver spi_peripheral_driver = {
	.probe = spi_peripheral_probe,
	.remove = spi_peripheral_remove,
	.driver = {
		.name = SPI_PERIPHERAL_NAME,
		.of_match_table = spi_peripheral_of_match,
	},
};
module_platform_driver(spi_peripheral_driver);

MODULE_AUTHOR("Alessio Alberti. <alberti@eurecom.fr>");
MODULE_DESCRIPTION("SPI-peripheral driver");
MODULE_LICENSE("GPL");
