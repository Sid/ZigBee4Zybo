--	This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--    This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--   You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.axi_pkg.all;

entity SPI_peripheral is
  port(
    aclk:       in std_logic;  -- Clock
    aresetn:    in std_logic;  -- Synchronous, active low, reset
    sclk:	out std_logic;
    mosi:	out std_logic;
    miso:	in std_logic;
    irq:	out std_logic;
    slave_sel:  out std_ulogic_vector(0 downto 0);
    --------------------------------
    -- AXI lite slave port s0_axi --
    --------------------------------
    -- Inputs (master to slave) --
    ------------------------------
    -- Read address channel
    s0_axi_araddr:  in  std_logic_vector(29 downto 0);
    s0_axi_arprot:  in  std_logic_vector(2 downto 0);
    s0_axi_arvalid: in  std_logic;
    -- Read data channel
    s0_axi_rready:  in  std_logic;
    -- Write address channel
    s0_axi_awaddr:  in  std_logic_vector(29 downto 0);
    s0_axi_awprot:  in  std_logic_vector(2 downto 0);
    s0_axi_awvalid: in  std_logic;
    -- Write data channel
    s0_axi_wdata:   in  std_logic_vector(31 downto 0);
    s0_axi_wstrb:   in  std_logic_vector(3 downto 0);
    s0_axi_wvalid:  in  std_logic;
    -- Write response channel
    s0_axi_bready:  in  std_logic;
    -------------------------------
    -- Outputs (slave to master) --
    -------------------------------
    -- Read address channel
    s0_axi_arready: out std_logic;
    -- Read data channel
    s0_axi_rdata:   out std_logic_vector(31 downto 0);
    s0_axi_rresp:   out std_logic_vector(1 downto 0);
    s0_axi_rvalid:  out std_logic;
    -- Write address channel
    s0_axi_awready: out std_logic;
    -- Write data channel
    s0_axi_wready:  out std_logic;
    -- Write response channel
    s0_axi_bresp:   out std_logic_vector(1 downto 0);
    s0_axi_bvalid:  out std_logic
  );
end entity SPI_peripheral;

architecture rtl of SPI_peripheral is

  -- Record versions of AXI signals
  signal s0_axi_m2s: axilite_gp_m2s;
  signal s0_axi_s2m: axilite_gp_s2m;
  signal s1_axi_m2s: axi_gp_m2s;
  signal s1_axi_s2m: axi_gp_s2m;
  signal m_axi_m2s: axi_gp_m2s;
  signal m_axi_s2m: axi_gp_s2m;

  -- Peripheral registers
  signal tx_data_reg: 		std_ulogic_vector(31 downto 0);
  signal rx_data_reg: 		std_ulogic_vector(31 downto 0);
  signal clk_divisor_reg: 	std_ulogic_vector(31 downto 0);
  signal conf_reg: 		std_ulogic_vector(31 downto 0);
  signal sw_rst:	std_ulogic;
  -- alis definition, bit field explanation of conf_reg
  alias cpha:    	  std_ulogic is conf_reg(0);
  alias cpol:    	  std_ulogic is conf_reg(1);
  alias ack:    	  std_ulogic is conf_reg(2);
  alias enable:    	  std_ulogic is conf_reg(3);
  alias async_rst:    std_ulogic is conf_reg(4);
  alias slave_reg:   std_ulogic_vector(0 downto 0) is conf_reg(5 downto 5);
  --alias ackn:    	  std_ulogic is conf_reg(7);
  --alias int:    	  std_ulogic is conf_reg(8);

  signal sig: std_logic;
  signal cnt: integer := 0;
  signal ack_d1,ack_strb,ack_rs,en_dl,en_strb,en_rs,rst_rs,rst_dl,rst_strb: std_ulogic;

  -- Or reduction of std_ulogic_vector
  function or_reduce(v: std_ulogic_vector) return std_ulogic is
    variable tmp: std_ulogic_vector(v'length - 1 downto 0) := v;
  begin
    if tmp'length = 0 then
      return '0';
    elsif tmp'length = 1 then
      return tmp(0);
    else
      return or_reduce(tmp(tmp'length - 1 downto tmp'length / 2)) or
             or_reduce(tmp(tmp'length / 2 - 1 downto 0));
    end if;
  end function or_reduce;

begin
  sw_rst <= rst_rs or not aresetn;
  -- instantiation spi controller as entity
  SPI_controller: entity work.spi_master(bhv)
	port map(clock			=> aclk,
    		 asyncRst		=> sw_rst,
    		 enable			=> en_rs,
    		 cpol			=> cpol,
    		 cpha			=> cpha,
    		 clk_div		=> clk_divisor_reg,
    		 slave_reg		=> slave_reg,
    		 tx_data		=> tx_data_reg(7 downto 0),
    		 miso			=> miso,
    		 sclk		    => sclk,
    		 slave_select   => slave_sel,
    		 mosi			=> mosi,
		 irq			=> irq,
		 ack			=> ack_rs,
   		 rx_data		=> rx_data_reg
	);
  -- S0_AXI read-write requests
  s0_axi_pr: process(aclk)
    -- idle: waiting for AXI master requests: when receiving write address and data valid (higher priority than read), perform the write, assert write address
    --       ready, write data ready and bvalid, go to w1, else, when receiving address read valid, perform the read, assert read address ready, read data valid
    --       and go to r1
    -- w1:   deassert write address ready and write data ready, wait for write response ready: when receiving it, deassert write response valid, go to idle
    -- r1:   deassert read address ready, wait for read response ready: when receiving it, deassert read data valid, go to idle
    type state_type is (idle, w1, r1);
    variable state: state_type;
  begin
    if rising_edge(aclk) then
      if aresetn = '0' then
        s0_axi_s2m <= (rdata => (others => '0'), rresp => axi_resp_okay, bresp => axi_resp_okay, others => '0');
        state := idle;
        ack_d1   <= '0';
        en_dl   <= '0';
        ack_rs  <= '0';
        en_rs   <= '0';
        rst_rs  <= '0';
        tx_data_reg <= (others => '0');
        clk_divisor_reg <= (others => '0');
        conf_reg <= (others => '0');
      else
	ack_d1   <= ack;
	en_dl    <= enable;
	ack_rs  <= ack_strb;
    	en_rs   <= en_strb;
   	rst_rs  <= rst_strb;
    	rst_dl  <= async_rst;
	if(ack_rs = '1') then
		ack <= '0';
	end if;
	if(en_rs = '1') then
        enable <= '0';
    end if;
    if(rst_rs = '1') then
        async_rst <= '0';
    end if;
        -- s0_axi write and read
        case state is
          when idle =>
           if s0_axi_m2s.awvalid = '1' and s0_axi_m2s.wvalid = '1' then -- Write address and data
              if or_reduce(s0_axi_m2s.awaddr(31 downto 4)) /= '0' then -- If unmapped address
                s0_axi_s2m.bresp <= axi_resp_decerr;
			  -- error if we try to write a read-only register as rx_data_reg
              elsif s0_axi_m2s.awaddr(3 downto 2) = "01" then -- If read-only rx_data_reg
                s0_axi_s2m.bresp <= axi_resp_slverr;
              else
                s0_axi_s2m.bresp <= axi_resp_okay;
                for i in 0 to 3 loop
                  if s0_axi_m2s.wstrb(i) = '1' then
				--register selection based on certain range of the write address
				case s0_axi_m2s.awaddr(3 downto 2) is
					when "00" =>
						tx_data_reg(8 * i + 7 downto 8 * i) <= s0_axi_m2s.wdata(8 * i + 7 downto 8 * i);
					when "10" =>
						clk_divisor_reg(8 * i + 7 downto 8 * i) <= s0_axi_m2s.wdata(8 * i + 7 downto 8 * i);
					when "11" =>
						conf_reg(8 * i + 7 downto 8 * i) <= s0_axi_m2s.wdata(8 * i + 7 downto 8 * i);		
					when others =>
						s0_axi_s2m.bresp <= axi_resp_slverr;
					end case;
                  end if;
                end loop;
              end if; 
              s0_axi_s2m.awready <= '1';
              s0_axi_s2m.wready <= '1';
              s0_axi_s2m.bvalid <= '1';
              state := w1;
            elsif s0_axi_m2s.arvalid = '1' then
              if or_reduce(s0_axi_m2s.araddr(31 downto 4)) /= '0' then -- If unmapped address
                s0_axi_s2m.rdata <= (others => '0');
                s0_axi_s2m.rresp <= axi_resp_decerr;
              else
                s0_axi_s2m.rresp <= axi_resp_okay;
				-- if statement to choose the correct register based on 2 bot of addresses (0,4,8 and 12)
				-- are the possible offset with respect to base address
                		if s0_axi_m2s.araddr(3 downto 2) = "00" then -- If status register
                  		  s0_axi_s2m.rdata <= tx_data_reg;
				elsif s0_axi_m2s.araddr(3 downto 2) = "01" then
				  s0_axi_s2m.rdata <= rx_data_reg;
				elsif s0_axi_m2s.araddr(3 downto 2) = "10" then
				  s0_axi_s2m.rdata <= clk_divisor_reg;
                else -- s0_axi_m2s.araddr(3 downto 2) = "11"
                  s0_axi_s2m.rdata <= conf_reg;
                end if;
              end if;
              s0_axi_s2m.arready <= '1';
              s0_axi_s2m.rvalid <= '1';
              state := r1;
            end if;
          when w1 =>
            s0_axi_s2m.awready <= '0';
            s0_axi_s2m.wready <= '0';
            if s0_axi_m2s.bready = '1' then
              s0_axi_s2m.bvalid <= '0';
              state := idle;
            end if;
          when r1 =>
            s0_axi_s2m.arready <= '0';
            if s0_axi_m2s.rready = '1' then
              s0_axi_s2m.rvalid <= '0';
              state := idle;
            end if;
        end case;
      end if;
    end if;
  end process s0_axi_pr;
  
   ack_strb <= ack and not ack_d1;
   en_strb <= enable and not en_dl;
   rst_strb <= async_rst and not rst_dl;

-- Record types to flat signals
  s0_axi_m2s.araddr  <= std_ulogic_vector("00" & s0_axi_araddr);
  s0_axi_m2s.arprot  <= std_ulogic_vector(s0_axi_arprot);
  s0_axi_m2s.arvalid <= s0_axi_arvalid;

  s0_axi_m2s.rready  <= s0_axi_rready;

  s0_axi_m2s.awaddr  <= std_ulogic_vector("00" & s0_axi_awaddr);
  s0_axi_m2s.awprot  <= std_ulogic_vector(s0_axi_awprot);
  s0_axi_m2s.awvalid <= s0_axi_awvalid;

  s0_axi_m2s.wdata   <= std_ulogic_vector(s0_axi_wdata);
  s0_axi_m2s.wstrb   <= std_ulogic_vector(s0_axi_wstrb);
  s0_axi_m2s.wvalid  <= s0_axi_wvalid;

  s0_axi_m2s.bready  <= s0_axi_bready;

  s0_axi_arready     <= s0_axi_s2m.arready;

  s0_axi_rdata       <= std_logic_vector(s0_axi_s2m.rdata);
  s0_axi_rresp       <= std_logic_vector(s0_axi_s2m.rresp);
  s0_axi_rvalid      <= s0_axi_s2m.rvalid;

  s0_axi_awready     <= s0_axi_s2m.awready;

  s0_axi_wready      <= s0_axi_s2m.wready;

  s0_axi_bvalid      <= s0_axi_s2m.bvalid;
  s0_axi_bresp       <= std_logic_vector(s0_axi_s2m.bresp);

end architecture rtl;

