--	This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--    This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--   You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY spi_master IS
  GENERIC(
    n_slaves  : POSITIVE := 1;  --number of spi slaves
	--data_bits already contain address, mode r/w
    data_bits : POSITIVE := 32); --data bus width
	-- bits for clock divisor
  PORT(
    clock    : IN     STD_ULOGIC;                             --system clock
    asyncRst : IN     STD_ULOGIC;                             --asynchronous reset
    enable   : IN     STD_ULOGIC;                             --initiate transaction
    cpol     : IN     STD_ULOGIC;                             --spi clock polarity
    cpha     : IN     STD_ULOGIC;                             --spi clock phase
    clk_div  : IN     STD_ULOGIC_VECTOR(31 downto 0);         --system clock cycles per half period of SCKL
    slave_reg : IN     STD_ULOGIC_VECTOR(n_slaves-1 DOWNTO 0);          --address of slave log2(#slaves)
    tx_data  : IN     STD_ULOGIC_VECTOR(7 DOWNTO 0);  --data to transmit sent by CPU
    miso     : IN     STD_ULOGIC;                             --master in, slave out
    sclk     : OUT STD_ULOGIC;                             --spi clock
    slave_select  : OUT STD_ULOGIC_VECTOR(n_slaves-1 DOWNTO 0);   --slave select
    mosi     : OUT    STD_ULOGIC;                             --master out, slave in
    irq	     : OUT    STD_ULOGIC;				--irq signal
    ack	     : IN     STD_ULOGIC;				--ack signal
    rx_data  : OUT    STD_ULOGIC_VECTOR(data_bits-1 DOWNTO 0)); --data received
END spi_master;

ARCHITECTURE bhv OF spi_master IS
  TYPE machine IS(ready, execute);                            --state machine data type, just 2 states
  SIGNAL state       : machine := ready;                               --current state
  SIGNAL sel_slave   : NATURAL range 0 to n_slaves-1;           --slave selected for current transaction
  SIGNAL max   		 : NATURAL range 0 to integer'high - 1;  --current clk_div, keep its value when fsm is ready
  SIGNAL count       : NATURAL range 0 to integer'high - 1;  --counter to trigger sclk from system clock
  SIGNAL clk_toggles : NATURAL RANGE 0 TO 2*8 + 1;    --count spi clock toggles
  -- to alternate tx and rx, tx whe assert is 1 and rx when it's 0
  -- it is based on the cpha, if cpha=0 sample on 1st togle of spi
  -- if cpha=1 sample from 2nd toggle to the last one
  SIGNAL assert_data : STD_ULOGIC;                            --'1' is tx sclk toggle, '0' is rx sclk toggle
  SIGNAL rx_buffer   : STD_ULOGIC_VECTOR(7 DOWNTO 0); --receive data buffer
  SIGNAL tx_buffer   : STD_ULOGIC_VECTOR(7 DOWNTO 0); --transmit data buffer
  SIGNAL last_bit_rx : NATURAL RANGE 0 TO 8*2;         --last rx data bit location
  SIGNAL nBits	 	 : POSITIVE := 8;
 ----
  SIGNAL s : STD_ULOGIC;

BEGIN

  sclk <= s;
  slave_select <= slave_reg;

  PROCESS(clock, asyncRst)
  BEGIN
    IF(asyncRst = '1') THEN      --reset system
      irq <= '0';                --set irq signal
      mosi <= '0';               --set master out to high impedance
      rx_data <= (OTHERS => '0');--clear receive data port
      state <= ready;             --go to ready state when reset is exited
      tx_buffer <= (OTHERS => '0');
      rx_buffer <= (OTHERS => '0');
      s <= '0';
    ELSIF(clock'EVENT AND clock = '1') THEN
      CASE state IS               --state machine
        WHEN ready =>
	  IF (ack = '1') THEN
          	irq <= '0';             --clock out not busy signal
	  END IF;
          mosi <= '0';             --set mosi output high impedance
          --user input to initiate transaction
          IF(enable = '1') THEN       
            IF(conv_integer(unsigned(clk_div)) = 0) THEN     --check for valid spi speed
              max <= 0;        --set to maximum speed if zero
              count <= 0;            --initiate system-to-spi clock counter
            ELSE
              max <= conv_integer(unsigned(clk_div)) - 1;  --set to input selection if valid
              count <= conv_integer(unsigned(clk_div)) - 1;      --initiate system-to-spi clock counter
            END IF;
            s <= cpol;            --set spi clock polarity
            assert_data <= NOT cpha; --set spi clock phase
            tx_buffer <= tx_data;   --clock in data for transmit into buffer
            clk_toggles <= 0;        --initiate clock toggle counter
	    --if it is 0 is a short  addres mode, 1 long address one
	    last_bit_rx <= 8*2 + conv_integer(cpha) -1; --set last rx data bit
	    rx_buffer <= (OTHERS => '0');
            state <= execute;        --proceed to execute state
          ELSE
            state <= ready;          --remain in ready state
          END IF;
		
		-- when fsm is in execute state, it doesn't depend on enable signal
        WHEN execute =>
          irq <= '0';        --set busy signal
          
          --system clock to sclk ratio is met
          IF(count = max) THEN        
            count <= 0;                     --reset system-to-spi clock counter
            assert_data <= NOT assert_data; --switch transmit/receive indicator
            
	    -- all clock toggles
	    --IF(ss(conv_integer(unsigned(slave_addr))) = '0') THEN
	     IF(clk_toggles = nBits*2) THEN
		  clk_toggles <= 0;               --reset spi clock toggles counter
	     ELSE
		  clk_toggles <= clk_toggles + 1; --increment spi clock toggles counter
	     END IF;
            --END IF;

            --spi clock generation
            --IF(clk_toggles < 2*nBits AND ss(conv_integer(unsigned(slave_addr))) = '0') THEN 
	    IF(clk_toggles /= 0 and clk_toggles <= 2*nBits) THEN 
              s <= NOT s; --toggle spi clock
            END IF;
            
	    --transmit spi clock toggle
	    IF(assert_data = '1' AND clk_toggles < last_bit_rx) THEN
	  	mosi <= tx_buffer(7);                     --clock out data bit 
		tx_buffer <= tx_buffer(6 DOWNTO 0) & '0'; --shift data transmit buffer
	    END IF;
            --receive spi clock toggle
            IF(assert_data = '0' AND clk_toggles < last_bit_rx + 1 and clk_toggles /= 0) THEN
		rx_buffer <= rx_buffer(6 DOWNTO 0) & miso; --shift in received bit
            END IF;

	    --end of transaction
          IF(clk_toggles = 2*nBits) THEN
	        mosi <= '0';
		rx_data <= "000000000000000000000000" & rx_buffer(7 DOWNTO 0); --clock out received data to output port
	      irq <= '1';             --clock out not busy signal
              state <= ready;          --return to ready state 
	    ELSE
              state <= execute;        --remain in execute state
            END IF;
          
          ELSE        --system clock to sclk ratio not met
            count <= count + 1; --increment counter
            state <= execute;   --remain in execute state
          END IF;

      END CASE;
    END IF;
  END PROCESS; 
END bhv;

