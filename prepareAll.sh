#!/bin/sh

if [ "$#" -eq 0 -o "$#" -gt 2 ]; then
    echo "Illegal number of parameters"
	echo "USAGE: ./prepareAll.sh <operation> <name-of-SD-card>(assumed mounted in media)"
	echo "Allowed operations:\nrootfs\nkernel\nuboot\ndts\nfsbl\nsd"
else
	if [ "$1" = "rootfs" ]
	then
		mkdir -p $SPI_P/build/rootfs
		touch $SPI_P/build/rootfs/external.mk $SPI_P/build/rootfs/Config.in
		cd $BUILDROOT
		make BR2_EXTERNAL=$SPI_P/build/rootfs O=$SPI_P/build/rootfs zynq_zed_defconfig	
		cp $SPI_P/ConfigRootFs $SPI_P/build/rootfs/.config 
		mkdir -p $SPI_P/build/rootfs/overlays/etc/profile.d
		echo "export PS1='Spi_p> '" > $SPI_P/build/rootfs/overlays/etc/profile.d/prompt.sh
		cp $SPI_P/scripts/init.sh $SPI_P/build/rootfs/overlays
		cd $SPI_P/build/rootfs
		make 
	elif [ "$1" = "kernel" ]
	then
		export PATH=$PATH:$SPI_P/build/rootfs/host/usr/bin
		export CROSS_COMPILE=arm-buildroot-linux-uclibcgnueabi-
		cd $XLINUX
		make O=$SPI_P/build/kernel ARCH=arm xilinx_zynq_defconfig
		cp -f $SPI_P/ConfigKernel $SPI_P/build/kernel/.config
		cd $SPI_P/build/kernel
		make -j8 ARCH=arm
		make ARCH=arm LOADADDR=0x8000 uImage
		make -j8 ARCH=arm modules
		make -j8 ARCH=arm modules_install INSTALL_MOD_PATH=$SPI_P/build/rootfs/overlays
		make headers_install ARCH=arm INSTALL_HDR_PATH=$SPI_P/build/rootfs/overlays
		cd $SPI_P/C
		make
		cd $SPI_P
		wget http://www.infradead.org/~tgr/libnl/files/libnl-3.2.24.tar.gz
		tar -xzf libnl-3.2.24.tar.gz
		cd libnl-3.2.24
		./configure --host=arm-linux --prefix=$SPI_P/build/rootfs/overlays
		make
		make install
		git clone git://git.code.sf.net/p/linux-zigbee/linux-zigbee linux-zigbee
		cd linux-zigbee
		./autogen.sh
		./configure --prefix=$SPI_P/build/rootfs/overlays --host=arm-linux PKG_CONFIG_PATH=$SPI_P/build/rootfs/overlays/lib/pkgconfig
		make
		make install
		rm -rf $SPI_P/libnl-3.2.24.tar.gz
		cd $SPI_P/build/rootfs
		make
	elif [ "$1" = "uboot" ]
	then
		cd $XUBOOT
		make mrproper
		make O=$SPI_P/build/uboot zynq_zybo_defconfig
		cd $SPI_P/build/uboot
		make -j8
		cp $SPI_P/build/uboot/u-boot $SPI_P/build/uboot/u-boot.elf
	elif [ "$1" = "dts" ]
	then
		cd $SPI_P
		make XDTS=$XDTS dts
		cp -f $SPI_P/pl.dtsi $SPI_P/build/dts/pl.dtsi
		dtc -I dts -O dtb -o build/devicetree.dtb build/dts/system.dts
	elif [ "$1" = "fsbl" ]
	then
		cd $SPI_P
		make fsbl
		make -C build/fsbl
		bootgen -w -image scripts/boot.bif -o build/boot.bin
	elif [ "$1" = "sd" -a "$#" -eq 2 ]
	then
		cd $SPI_P
		cp build/boot.bin build/devicetree.dtb build/kernel/arch/arm/boot/uImage /media/$USER/$2
		cp build/rootfs/images/rootfs.cpio.uboot /media/$USER/$2/uramdisk.image.gz
		sync
		umount /media/$USER/$2
	else
		echo "wrong parameter!!"
	fi
	
fi

